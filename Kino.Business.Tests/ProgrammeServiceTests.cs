﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using Kino.Business.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business.Tests
{
    [TestFixture]
    public class ProgrammeServiceTests
    {
        [Test]
        public void DoesMovieIsPlayedBeforeCinemaOpening_ErlierHour_ExceptionThrown()
        {
            //Arrange
            var openHour = TimeSpan.Parse("07:00");

            var seanceHour = TimeSpan.Parse("06:30");

            var cinema = new CinemaBl()
            {
                Name = "Helios",
                Adress = "Gdansk",
                Mail = "helios@gmail.com",
                Telephone = 123456789,
                OpenHour = openHour,
            };

            var hour = new HourBl()
            {
                Hour = seanceHour,
            };

            var hourList = new List<HourBl>() { hour };

            var programme = new ProgrammeBl()
            {
                Cinema = cinema,
                Hours = hourList,
            };

            var programmeService = new ProgrammeService(null, null, null, null);

            //Act and Assert

            var ex = Assert.Throws<Exception>(() => programmeService.DoesMovieIsPlayedBeforeCinemaOpening(programme));
            Assert.That(ex.Message, Is.EqualTo("Film nie moze sie zaczynac przed otwarciem kina!"));
        }

        [Test]
        public void DetermineDateForProgramme_StartDateTime_ExceptionThrown()
        {
            //Arrange

            DateTime startDate = DateTime.ParseExact("06/15/2019", "d", CultureInfo.InvariantCulture);

            var programmeService = new ProgrammeService(null, null, null, null);

            //Act and Assert

            var ex = Assert.Throws<Exception>(() => programmeService.DetermineDateForProgramme(null, startDate));
            Assert.That(ex.Message, Is.EqualTo("Tydzien zaczyna sie w piatek. Musisz podac date piatku"));
        }

        [Test]
        public void DoesMoviesTieInWith_InvalidInput_ExceptionThrown()
        {
            //Arrange
            var screeningRoom = new ScreeningRoomBl(){RoomNumber = 1};

            var hour = new HourBl(){Hour = TimeSpan.Parse("14:00")};
            var hourList = new List<HourBl>() { hour };

            var movie = new FilmBl(){LengthOfTheFilm = 120};

            var cinema = new CinemaBl(){Name = "Helios"};

            var programme = new ProgrammeBl()
            {
                StartDate = DateTime.ParseExact("04/26/2019", "d", CultureInfo.InvariantCulture),
                Film = movie,
                Cinema = cinema,
                Hours = hourList,
            };

            var lengthOfTheMovie = programme.Film.LengthOfTheFilm;
            var lengthOfTheMovieInTimeSpan = TimeSpan.FromMinutes(lengthOfTheMovie);

            var startingMovieHour = hour.Hour;
            var endingMovieHour = startingMovieHour.Add(lengthOfTheMovieInTimeSpan);

            var breakBetweenMovies = TimeSpan.FromMinutes(10);

            var screeningRoom1 = new ScreeningRoomBl() { RoomNumber = 1 };
            var screeningRoomList = new List<ScreeningRoomBl>() { screeningRoom1 };

            var hour1 = new HourBl() { Hour = TimeSpan.Parse("13:00"), ScreeningRoom = screeningRoom1 };
            var hour2 = new HourBl() { Hour = TimeSpan.Parse("18:00"), ScreeningRoom = screeningRoom1 };

            var hourList1 = new List<HourBl>() { hour1, hour2 };

            var programme1 = new ProgrammeBl()
            {
                Id = 1,
                EndDate = DateTime.ParseExact("04/26/2019", "d", CultureInfo.InvariantCulture),
                StartDate = DateTime.ParseExact("05/02/2019", "d", CultureInfo.InvariantCulture),
                Film = movie,
                Cinema = cinema,
                Hours = hourList1,
            };

            var hour3 = new HourBl() { Hour = TimeSpan.Parse("11:00"), ScreeningRoom = screeningRoom1 };
            var hour4 = new HourBl() { Hour = TimeSpan.Parse("20:00"), ScreeningRoom = screeningRoom1 };

            var hourList2 = new List<HourBl>() { hour1, hour2 };

            var programme2 = new ProgrammeBl()
            {
                Id = 2,
                EndDate = DateTime.ParseExact("04/26/2019", "d", CultureInfo.InvariantCulture),
                StartDate = DateTime.ParseExact("04/26/2019", "d", CultureInfo.InvariantCulture),
                Film = movie,
                Cinema = cinema,
                Hours = hourList2,
            };

            var programmesForCinema = new List<ProgrammeBl>() { programme1, programme2};

            var programmeServiceMock = new Mock<ICinemaService>();
            programmeServiceMock.Setup(x => x.GetProgrammeForSelectedCinemaAndForDataAsync(programme)).ReturnsAsync(programmesForCinema);

            var programmeService = new ProgrammeService(null, null, programmeServiceMock.Object, null);

            //Act and Assert

            var ex = Assert.ThrowsAsync<Exception>(async () => await programmeService.DoesMoviesTieInWithAsync(programme, hour, screeningRoom));
            Assert.That(ex.Message, Is.EqualTo("W tym czasie, w tej sali juz jest zaplanowany inny seans"));
        }
    }
}
