﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using Kino.Business.Services;
using Kino.Data.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Kino.Business.Tests
{
    [TestFixture]
    public class TicketServiceTests
    {
        [Test]
        public void CalculatePriceWithPromotion_TenthTicketWasBoughtByTheSameClient_ValidPrice()
        {
            //Arrange
            DateTime dateTime = new DateTime(2019, 05, 18);
            string mail = "monika.skwiercz@gmail.com";

            var cinema = new CinemaBl() { Id = 1};
            var movie = new FilmBl() { Id = 1 };
            var client = new ClientBl() { MailAddress = mail };
            var hour1 = new HourBl() { Id = 1 };

            var clientServiceMock = new Mock<IClientService>();

            var ticketService = new TicketService(null, null, clientServiceMock.Object);

            double price = ticketService.CalculatePriceOfTheTicketByDay(dateTime);

            var ticket1 = new TicketBl() { Id = 1, Cinema = cinema, Client = client, Film = movie, Hour = hour1, Row = "A", Seat = 10, DateTime = dateTime, Price = price};
            var ticket2 = new TicketBl() { Id = 2, Cinema = cinema, Client = client, Film = movie, Hour = hour1, Row = "B", Seat = 10, DateTime = dateTime, Price = price };
            var ticket3 = new TicketBl() { Id = 3, Cinema = cinema, Client = client, Film = movie, Hour = hour1, Row = "C", Seat = 10, DateTime = dateTime, Price = price };
            var ticket4 = new TicketBl() { Id = 4, Cinema = cinema, Client = client, Film = movie, Hour = hour1, Row = "D", Seat = 10, DateTime = dateTime, Price = price };
            var ticket5 = new TicketBl() { Id = 5, Cinema = cinema, Client = client, Film = movie, Hour = hour1, Row = "E", Seat = 10, DateTime = dateTime, Price = price };
            var ticket6 = new TicketBl() { Id = 6, Cinema = cinema, Client = client, Film = movie, Hour = hour1, Row = "F", Seat = 10, DateTime = dateTime, Price = price };
            var ticket7 = new TicketBl() { Id = 7, Cinema = cinema, Client = client, Film = movie, Hour = hour1, Row = "G", Seat = 10, DateTime = dateTime, Price = price };
            var ticket8 = new TicketBl() { Id = 8, Cinema = cinema, Client = client, Film = movie, Hour = hour1, Row = "H", Seat = 10, DateTime = dateTime, Price = price };
            var ticket9 = new TicketBl() { Id = 9, Cinema = cinema, Client = client, Film = movie, Hour = hour1, Row = "I", Seat = 10, DateTime = dateTime, Price = price };

            List<TicketBl> tickets = new List<TicketBl>() { ticket1, ticket2, ticket3, ticket4, ticket5, ticket6, ticket7, ticket8, ticket9 };

            IEnumerable<TicketBl> ticketsEnumerator = tickets;

            clientServiceMock.Setup(x => x.GetAllTicketsForClientAsync(mail)).ReturnsAsync(ticketsEnumerator);

            //Act

            var result = ticketService.CalculatePriceWithPropmotion(dateTime, mail).Result;

            //Assert

            Assert.AreEqual(0, result);
        }

        [Test]
        public void CalculatePriceWithPromotion_FifthTicketWasBoughtByTheSameClientInTheSameMonth_ValidPrice()
        {
            //Arrange
            DateTime dateTime = new DateTime(2019, 05, 18);
            string mail = "monika.skwiercz@gmail.com";

            var cinema = new CinemaBl() { Id = 1 };
            var movie = new FilmBl() { Id = 1 };
            var client = new ClientBl() { MailAddress = mail };
            var hour1 = new HourBl() { Id = 1 };

            var clientServiceMock = new Mock<IClientService>();

            var ticketService = new TicketService(null, null, clientServiceMock.Object);

            double price = ticketService.CalculatePriceOfTheTicketByDay(dateTime);

            var ticket1 = new TicketBl() { Id = 1, Cinema = cinema, Client = client, Film = movie, Hour = hour1, Row = "A", Seat = 10, DateTime = dateTime, Price = price };
            var ticket2 = new TicketBl() { Id = 2, Cinema = cinema, Client = client, Film = movie, Hour = hour1, Row = "B", Seat = 10, DateTime = dateTime, Price = price };
            var ticket3 = new TicketBl() { Id = 3, Cinema = cinema, Client = client, Film = movie, Hour = hour1, Row = "C", Seat = 10, DateTime = dateTime, Price = price };
            var ticket4 = new TicketBl() { Id = 4, Cinema = cinema, Client = client, Film = movie, Hour = hour1, Row = "D", Seat = 10, DateTime = dateTime, Price = price };
            var ticket5 = new TicketBl() { Id = 5, Cinema = cinema, Client = client, Film = movie, Hour = hour1, Row = "E", Seat = 10, DateTime = dateTime, Price = price };

            List<TicketBl> tickets = new List<TicketBl>() { ticket1, ticket2, ticket3, ticket4, ticket5};

            IEnumerable<TicketBl> ticketsEnumerator = tickets;

            clientServiceMock.Setup(x => x.GetAllTicketsForClientAsync(mail)).ReturnsAsync(ticketsEnumerator);

            //Act

            var result = ticketService.CalculatePriceWithPropmotion(dateTime, mail).Result;

            //Assert

            Assert.AreEqual(20, result);
        }

        [TestCase(2019, 5, 20, 20.0)]
        [TestCase(2019, 5, 21, 20.0)]
        [TestCase(2019, 5, 22, 20.0)]
        [TestCase(2019, 5, 23, 20.0)]
        [TestCase(2019, 5, 24, 25.0)]
        [TestCase(2019, 5, 25, 25.0)]
        [TestCase(2019, 5, 26, 25.0)]
        public void CalculatePriceOfTheTicket_FromMondayToThursday_ValidPrice(int year, int month, int day, double expectedResult)
        {
            //Arrange
            var dateTime = new DateTime(year, month, day);
            var ticketService = new TicketService(null, null, null);

            //Act

            var result = ticketService.CalculatePriceOfTheTicketByDay(dateTime);

            //Assert

            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void IsRowExist_LetterOfTheRow_ExceptionThrown()
        {
            //Arrange

            string row = "Z";
            var screeningRoom = new ScreeningRoomBl() { Id = 1, AmountOfRow = 20};

            var screeningRoomServiceMock = new Mock<IScreeningRoomService>();

            screeningRoomServiceMock.Setup(x => x.LetterToInt(row)).Returns(26);

            var ticketService = new TicketService(null, screeningRoomServiceMock.Object, null);

            //Act and Assert

            var ex = Assert.Throws<Exception>(() => ticketService.IsRowExist(screeningRoom, row));
            Assert.That(ex.Message, Is.EqualTo("There is not such row in this screening room"));
        }

        [TestCase(11, "There are not so much seats in the row")]
        [TestCase(0, "0 cannot be less then or equal 0")]
        public void IsSeatExist_SeatNumber_ExceptionThrown(int seat, string expectedException)
        {
            //Arrange

            var screeningRoom = new ScreeningRoomBl() { Id = 1, SeatsInRow = 10};

            var ticketService = new TicketService(null, null, null);

            //Act and assert

            var ex = Assert.Throws<Exception>(() => ticketService.IsSeatExist(screeningRoom, seat));
            Assert.That(ex.Message, Is.EqualTo(expectedException));
        }

    }
}
