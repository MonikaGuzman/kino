﻿using Kino.Business.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kino.Business.Interfaces
{
    public interface ICinemaService
    {
        Task AddCinemaAsync(CinemaBl cinema);
        Task<bool> DoesCinemaExistAsync(CinemaBl cinema);
        Task<IEnumerable<CinemaBl>> GetAllCinemasAsync();
        Task<bool> DoesTheSameScreemingRoomExistAsync(ScreeningRoomBl screeningRoom, CinemaBl cinema);
        bool DoesAdressMailFormatIsCorrect(string cinemaName);
        bool DoesTelephoneNumberIsCorrect(CinemaBl cinema);
        Task<IEnumerable<ScreeningRoomBl>> FindScreenignRoomForCinemaAsync(CinemaBl cinema);
        Task<IEnumerable<ProgrammeBl>> GetProgrammeForSelectedCinemaAndForDataAsync(ProgrammeBl programme);
    }
}