﻿using Kino.Business.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kino.Business.Interfaces
{
    public interface IClientService
    {
        Task AddClient(ClientBl client);
        Task<bool> DoesTheSameClientMailExistAsync(ClientBl client);
        Task DoesEmailExistAsync(string mail);
        Task<ClientBl> GetClientByMailAsync(string mail);
        Task<IEnumerable<TicketBl>> GetAllTicketsForClientAsync(string clientMail);
    }
}