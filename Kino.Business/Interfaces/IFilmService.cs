﻿using Kino.Business.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kino.Business.Interfaces
{
    public interface IFilmService
    {
        Task AddFilmAsync(FilmBl film);
        Task<bool> DoesTitleExistAsync(FilmBl film);
        bool DoesGenreExist(FilmBl film);
        bool DoesWebsiteHasCorrectFormat(FilmBl film);
        Task<IEnumerable<FilmBl>> GetAllMoviesAsync();
        FilmBl GetMovieForProgramme(List<ProgrammeBl> programmes);
    }
}