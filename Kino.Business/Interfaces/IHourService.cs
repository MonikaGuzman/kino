﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Kino.Business.Models;

namespace Kino.Business.Interfaces
{
    public interface IHourService
    {
        Task<IEnumerable<HourBl>> GetAllHoursAsync();
        Task<HourBl> GetHourByIdAsync(int id);
    }
}