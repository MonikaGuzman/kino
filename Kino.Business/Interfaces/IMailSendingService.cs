﻿using Kino.Business.Models;
using System.Collections.Generic;

namespace Kino.Business.Interfaces
{
    public interface IMailSendingService
    {
        void MailSend(OrderBl order, List<TicketBl> tickets);
        string GetMailSender(List<TicketBl> tickets);
        string GetMailReceiver(List<TicketBl> tickets);
    }
}