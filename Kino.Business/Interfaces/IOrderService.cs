﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Kino.Business.Models;
using Kino.Business.Services;

namespace Kino.Business.Interfaces
{
    public interface IOrderService
    {
        int AmountOfTheTickets(List<TicketBl> tickets);
        double CalculateOrder(List<TicketBl> tickets);
        Task OrderAddAsync(OrderBl order, List<TicketBl> tickets);
    }
}