﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Kino.Business.Models;
using Kino.Data.Models;

namespace Kino.Business.Interfaces
{
    public interface IProgrammeService
    {
        Task<bool> DoesMovieWasPlayedBeforeInCinemaAsync(ProgrammeBl programme);
        Task<IEnumerable<ProgrammeBl>> GetAllProgrammesAsync();
        Task<DateTime> LastDataOfPlayedMovieAsync(ProgrammeBl programme);
        Task AddProgrammeAsync(ProgrammeBl programme);
        void ImportProgrammeToJson(List<ProgrammeBl> programmes, string path);
        void ImportProgrammeToXml(List<ProgrammeBl> programmes, string path);
        Task<bool> DoesExistedCinemaWasChosenAsync(CinemaBl cinemaName);
        Task<List<ProgrammeBl>> GetProgrammeByCinemaNameAndDateAsync(CinemaBl cinemaName, DateTime data);
        Task<CinemaBl> GetCinemaByNameAsync(string name);
        Task<FilmBl> GetMovieByTitleAsync(string title);
        Task<ScreeningRoomBl> ChooseScreeningRoomsByNumberAsync(string cinemaName, int roomNumber);
        void DoesThePathIsCorrect(string path);
        Task<CinemaBl> GetCinemaByIdAsync(int id);
        Task<FilmBl> GetMovieByIdAsync(int id);
        Task<ScreeningRoomBl> GetScreeningRoomsByIdAsync(int cinemaId, int roomId);
        void DoesMovieIsPlayedBeforeCinemaOpening(ProgrammeBl programme);
        Task DoesMoviesTieInWithAsync(ProgrammeBl programme, HourBl hour, ScreeningRoomBl screeningRoom);
        Task DetermineDateForProgrammeAsync(ProgrammeBl programme);
        void DetermineDateForProgramme(ProgrammeBl programme, DateTime startDate);
    }
}