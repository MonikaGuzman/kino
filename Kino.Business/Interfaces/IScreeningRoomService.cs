﻿using Kino.Business.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kino.Business.Interfaces
{
    public interface IScreeningRoomService
    {
        Task<IEnumerable<ScreeningRoomBl>> GetAllScreeningRoomsAsync();
        bool IsNegativeValueOfScreeningRoom(ScreeningRoomBl screeningRoom);
        bool IsMoreRowThanLatinLetters(int amountOfRow);
        int LetterToInt(string letter);
        string IntToLetters(int value);
        ScreeningRoomBl GetScreeningRoomForProgrammeAndHour(List<ProgrammeBl> programmes, TimeSpan hour);
        string GetLetterOfTheLastRow(ScreeningRoomBl screeningRoom);
    }
}