﻿using Kino.Business.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kino.Business.Interfaces
{
    public interface ITicketService
    {
        double CalculatePriceOfTheTicketByDay(DateTime date);
        Task<int> AddTicketAsync(TicketBl ticket);
        Task DoesPlaceIsFreeAsync(TicketBl ticket);        
        Task<double> CalculatePriceWithPropmotion(DateTime dateTime, string mail);
        void IsLetter(string checkIsLetter);
        void IsSeatExist(ScreeningRoomBl screeningRoom, int seat);
        void IsRowExist(ScreeningRoomBl screeningRoom, string row);
        List<TicketBl> AddTicketToList(TicketBl ticket);
        void CheckIfTicketHasCorrectData(List<TicketBl> tickets);
        void CheckIfUserHasAllNecessaryProperties(List<TicketBl> tickets);
    }
}