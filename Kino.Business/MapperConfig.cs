﻿using Kino.Business.Models;
using Kino.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business
{
    public class MapperConfig
    {
        public static void CreateMap()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<CinemaBl, Cinema>();
                cfg.CreateMap<Cinema, CinemaBl>();
                cfg.CreateMap<FilmBl, Film>();
                cfg.CreateMap<Film, FilmBl>();
                cfg.CreateMap<ProgrammeBl, Programme>();
                cfg.CreateMap<Programme, ProgrammeBl>();
                cfg.CreateMap<ScreeningRoomBl, ScreeningRoom>();
                cfg.CreateMap<ScreeningRoom, ScreeningRoomBl>();
                cfg.CreateMap<HourBl, HourD>();
                cfg.CreateMap<HourD, HourBl>();
                cfg.CreateMap<ClientBl, Client>();
                cfg.CreateMap<Client, ClientBl>();
                cfg.CreateMap<TicketBl, Ticket>();
                cfg.CreateMap<Ticket, TicketBl>();
                cfg.CreateMap<OrderBl, Order>();
                cfg.CreateMap<Order, OrderBl>();
            });
        }
    }
}
