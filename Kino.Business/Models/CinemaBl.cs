﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business.Models
{
    public class CinemaBl
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public string Mail { get; set; }
        public int Telephone { get; set; }
        public TimeSpan OpenHour { get; set; }
        public List<ScreeningRoomBl> ScreeningRooms { get; set; }
    }
}
