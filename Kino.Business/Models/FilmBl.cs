﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business.Models
{
    public class FilmBl
    {
        public enum Genre
        {
            Comedy,
            Drama,
            Horror,
            Fantasy
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public Genre FilmGenre { get; set; }
        public int LengthOfTheFilm { get; set; }
        public string Description { get; set; }
        public string Website { get; set; }
    }
}
