﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business.Models
{
    public class HourBl
    {
        public int Id { get; set; }
        public TimeSpan Hour { get; set; }
        public ScreeningRoomBl ScreeningRoom { get; set; }
    }
}
