﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business.Models
{
    public class OrderBl
    {
        public OrderBl()
        {
            TicketsId = new List<int>();
            Tickets = new List<TicketBl>();
        }

        public int Id { get; set; }
        public double TotalPrice { get; set; }
        public int AmountOfTheTicket { get; set; }
        public List<int> TicketsId { get; set; }
        public List<TicketBl> Tickets { get; set; }
    }
}
