﻿using Kino.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business.Models
{
    public class ProgrammeBl
    {
        public int Id { get; set; }
        public CinemaBl Cinema { get; set; }
        public FilmBl Film { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<HourBl> Hours { get; set; }
        
    }
}
