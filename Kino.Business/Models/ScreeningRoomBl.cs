﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business.Models
{
    public class ScreeningRoomBl
    {
        public int Id { get; set; }
        public int RoomNumber { get; set; }
        public int AmountOfRow { get; set; }
        public int SeatsInRow { get; set; }
        public CinemaBl Cinema { get; set; }
    }
}
