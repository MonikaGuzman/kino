﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business.Models
{
    public class TicketBl
    {
        public int Id { get; set; }
        public double Price { get; set; }
        public CinemaBl Cinema { get; set; }
        public FilmBl Film { get; set; }
        public DateTime DateTime { get; set; }
        public HourBl Hour { get; set; }
        public string Row { get; set; }
        public int Seat { get; set; }

        public ClientBl Client { get; set; }
    }
}
