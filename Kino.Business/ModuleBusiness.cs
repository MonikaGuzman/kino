﻿using Kino.Business.Interfaces;
using Kino.Business.Services;
using Kino.Data;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business
{
    public class ModuleBusiness : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IFilmService>().To<FilmService>();
            Kernel.Bind<ICinemaService>().To<CinemaService>();
            Kernel.Bind<IScreeningRoomService>().To<ScreeningRoomService>();
            Kernel.Bind<IProgrammeService>().To<ProgrammeService>();
            Kernel.Bind<IHourService>().To<HourService>();
            Kernel.Bind<IClientService>().To<ClientService>();
            Kernel.Bind<ITicketService>().To<TicketService>();
            Kernel.Bind<IOrderService>().To<OrderService>();
            Kernel.Bind<IMailSendingService>().To<MailSendingService>();
            Kernel.Load(new List<INinjectModule>() { new ModuleData() });
        }
    }
}
