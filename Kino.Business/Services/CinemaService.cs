﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using Kino.Data.Interfaces;
using Kino.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Kino.Business
{
    public class CinemaService : ICinemaService
    {
        private readonly Func<IDataContainer> _dataContainer;
        private IScreeningRoomService _screeningRoomService;

        public CinemaService(Func<IDataContainer> dataContainer, IScreeningRoomService screeningRoomService)
        {
            _dataContainer = dataContainer;
            _screeningRoomService = screeningRoomService;
        }
    
        public async Task AddCinemaAsync(CinemaBl cinema)
        {
            bool doesCinemaExist = await DoesCinemaExistAsync(cinema);
            bool doesAdressMailFormatIsCorrect = DoesAdressMailFormatIsCorrect(cinema.Mail);
            bool doesTelephoneNumberIsCorrect = DoesTelephoneNumberIsCorrect(cinema);

            if (!doesCinemaExist && doesAdressMailFormatIsCorrect && doesTelephoneNumberIsCorrect)
            {
                using (var dataContainer = _dataContainer())
                {
                    var cinemaToAdd = AutoMapper.Mapper.Map<CinemaBl, Cinema>(cinema);
                    dataContainer.Cinemas.Add(cinemaToAdd);
                    await dataContainer.SaveChangesRepoAsync();
                }
            }
        }

        public async Task<bool> DoesCinemaExistAsync(CinemaBl cinema)
        {
            using (var dataContainer = _dataContainer())
            {
                bool isCorrect = false;
                bool doesCinemaExist = await dataContainer.Cinemas.AnyAsync(c => c.Name == cinema.Name);

                if (doesCinemaExist)
                {
                    return isCorrect = true;
                    throw new ArgumentException("Taka nazwa kina juz istnieje");
                }

                if (isCorrect)
                {
                    return false;
                }

                return isCorrect;
            }
        }

        public async Task<IEnumerable<ScreeningRoomBl>> FindScreenignRoomForCinemaAsync(CinemaBl cinema)
        {
            var cinemas = await GetAllCinemasAsync();

            var screeningRooms = cinemas.Where(x => x.Name == cinema.Name).SelectMany(x => x.ScreeningRooms);

            return screeningRooms;
        }

        public async Task<bool> DoesTheSameScreemingRoomExistAsync(ScreeningRoomBl screeningRoom, CinemaBl cinema)
        {
            using (var dataContainer = _dataContainer())
            {
                bool returingResult = false;

                var doesTheSameScreeningRoomExist = await dataContainer.Cinemas.AnyAsync(c => c.Name == cinema.Name
                && c.ScreeningRooms.Any(s => s.RoomNumber == screeningRoom.RoomNumber));

                if (doesTheSameScreeningRoomExist)
                {
                    return returingResult = true;
                }

                return returingResult;
            }
        }

        public bool DoesAdressMailFormatIsCorrect(string cinemaName)
        {
            const string userNameGroupName = "username";
            const string domainGroupName = "domain";
            const string postfixGroupName = "postfix";
            const string countryGroupName = "country";

            var regex = new Regex(@"^(?<" + userNameGroupName +
                @">[\w\.-]{2,})@(?<" + domainGroupName +
                @">[\dA-z]{2,})\.((?<" + postfixGroupName +
                @">[\dA-z]{3})\.)?(?<" + countryGroupName + @">[\dA-z]{2,3})$");

            var match = regex.Match(cinemaName);

            if (!match.Success)
            {
                throw new ArgumentException($"{cinemaName} nie jest poprawnym mailem");
            }

            return match.Success;
        }

        public bool DoesTelephoneNumberIsCorrect(CinemaBl cinema)
        {
            var regex = new Regex(@"^(\+[0-9]{2})([0-9]{9})|([0-9]{9})|([0-9]{2})([0-9]{7})$");

            var match = regex.Match(cinema.Telephone.ToString());

            if (!match.Success)
            {
                throw new ArgumentException($"{cinema.Telephone} nie jest poprawnym numerem telefonu");
            }

            return match.Success;
        }

        public async Task<IEnumerable<CinemaBl>> GetAllCinemasAsync()
        {
            using (var dataContainer = _dataContainer())
            {
                var cinemasBl = (await dataContainer.Cinemas.Include(x => x.ScreeningRooms).ToListAsync())
                     .Select(c => AutoMapper.Mapper.Map<Cinema, CinemaBl>(c));
                return cinemasBl;
            }
        }

        public async Task<IEnumerable<ProgrammeBl>> GetProgrammeForSelectedCinemaAndForDataAsync(ProgrammeBl programme)
        {
            using (var dataContainer = _dataContainer())
            {
                var programmesForCinema = (await dataContainer.Programmes.Where(x => x.Cinema.Name == programme.Cinema.Name
                && x.StartDate == programme.StartDate).Include(m => m.Film).Include(y => y.Hours.Select(z => z.ScreeningRoom)).ToListAsync())
                .Select(p => AutoMapper.Mapper.Map<Programme, ProgrammeBl>(p));

                return programmesForCinema;
            }
        }
    }
}
