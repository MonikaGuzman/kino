﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using Kino.Data.Interfaces;
using Kino.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business.Services
{
    public class ClientService : IClientService
    {
        private readonly Func<IDataContainer> _dataContainer;
        private readonly ICinemaService _cinemaService;
  
        public ClientService(Func<IDataContainer> dataContainer, ICinemaService cinemaService)
        {
            _dataContainer = dataContainer;
            _cinemaService = cinemaService;
        }

        public async Task AddClient(ClientBl client)
        {
            bool doesEmailAdressIsCorrectFormat = _cinemaService.DoesAdressMailFormatIsCorrect(client.MailAddress);
            bool doesTheSameClientMailExist = await DoesTheSameClientMailExistAsync(client);

            using (var dataContainer = _dataContainer())
            {
                var clientToAdd = AutoMapper.Mapper.Map<ClientBl, Client>(client);
                dataContainer.Clients.Add(clientToAdd);
                await dataContainer.SaveChangesRepoAsync();
            }
        }

        public async Task<bool> DoesTheSameClientMailExistAsync(ClientBl client)
        {
            using (var dataContainer = _dataContainer())
            {
                var doesTheSameClientMailExist = await dataContainer.Clients.AnyAsync(x => x.MailAddress == client.MailAddress);

                if (!doesTheSameClientMailExist)
                {
                    return true;
                }

                if (doesTheSameClientMailExist)
                {
                    throw new Exception($"{client.MailAddress} is existed in system! E - mail must be uniqe");
                }

                return false;
            }
        }

        public async Task DoesEmailExistAsync(string mail)
        {
            using (var dataContainer = _dataContainer())
            {
                var doesEmailExist = await dataContainer.Clients.AnyAsync(x => x.MailAddress == mail);

                if (!doesEmailExist)
                {
                    throw new Exception($"{mail} does not exist!");
                }
            }
        }

        public async Task<ClientBl> GetClientByMailAsync(string mail)
        {
            using (var dataContainer = _dataContainer())
            {
                var client = await dataContainer.Clients.SingleOrDefaultAsync(x => x.MailAddress == mail);
                var clientBl = AutoMapper.Mapper.Map<Client, ClientBl>(client);

                return clientBl;
            }
        }

        public async Task<IEnumerable<TicketBl>> GetAllTicketsForClientAsync(string clientMail)
        {
            using (var dataContainer = _dataContainer())
            {
                var ticketsBl = (await dataContainer.Tickets.Include(x => x.Cinema).Include(y => y.Film)
                    .Include(z => z.Hour).Include(i => i.Client).ToListAsync()).Where(c => c.Client.MailAddress == clientMail)
                    .Select(t => AutoMapper.Mapper.Map<Ticket, TicketBl>(t));

                return ticketsBl;
            }
        }
    }
}
