﻿using AutoMapper;
using Kino.Business.Interfaces;
using Kino.Business.Models;
using Kino.Data.Interfaces;
using Kino.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Kino.Business.Services
{
    public class FilmService : IFilmService
    {
        private readonly Func<IDataContainer> _dataContainer;

        public FilmService(Func<IDataContainer> dataContainer)
        {
            _dataContainer = dataContainer;
        }

        public async Task AddFilmAsync(FilmBl film)
        {
            bool doesTitleExist = await DoesTitleExistAsync(film);
            bool doesGenreExist = DoesGenreExist(film);
            bool doesWebsiteExist = DoesWebsiteHasCorrectFormat(film);

            if (doesTitleExist && doesGenreExist && doesWebsiteExist)
            {
                using (var dataContainer = _dataContainer())
                {
                    var filmToAdd = AutoMapper.Mapper.Map<FilmBl, Film>(film);
                    dataContainer.Films.Add(filmToAdd);
                    await dataContainer.SaveChangesRepoAsync();
                }
            }
        }

        public async Task<bool> DoesTitleExistAsync(FilmBl film)
        {
            using (var dataContainer = _dataContainer())
            {
                bool isCorrect = true;
                bool doesFilmExist = await dataContainer.Films.AnyAsync(f => f.Title == film.Title);

                if (doesFilmExist)
                {
                    throw new ArgumentException("Taki film juz istnieje");
                }

                return isCorrect;
            }
        }

        public bool DoesGenreExist(FilmBl film)
        {
            bool isCorrect = true;

            bool doesGenreExists = Enum.IsDefined(typeof(FilmBl.Genre), film.FilmGenre);

            if (!doesGenreExists)
            {
                throw new ArgumentException("Taki gatunek nie istnieje");
            }

            if (string.IsNullOrWhiteSpace(film.FilmGenre.ToString()))
            {
                throw new ArgumentException("Nie mozna dodac filmu bez gatunku");
            }

            return isCorrect;

        }

        public bool DoesWebsiteHasCorrectFormat(FilmBl film)
        {
            bool isCorrect = true;

            var regex = new Regex(@"^(http|https):\/\/(www.)(filmweb).pl\/[a-zA-Z\/%0-9+-_]{0,}$");

            var match = regex.Match(film.Website);

            if (!match.Success)
            {
                throw new ArgumentException($"{film.Website} nie jest poprawnym adresem strony internetowej");
            }

            return isCorrect;
        }

        public async Task<IEnumerable<FilmBl>> GetAllMoviesAsync()
        {
            using (var dataContainer = _dataContainer())
            {
                var moviesBl = (await dataContainer.Films.ToListAsync()).Select(c => AutoMapper.Mapper.Map<Film, FilmBl>(c));
                return moviesBl;
            }
        }

        public FilmBl GetMovieForProgramme(List<ProgrammeBl> programmes)
        {
            var movie = new FilmBl();

            foreach (var programm in programmes)
            {
                movie = programm.Film;
                return movie;
            }

            return movie;
        }
    }
}
