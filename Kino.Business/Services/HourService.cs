﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using Kino.Data.Interfaces;
using Kino.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business.Services
{
    public class HourService : IHourService
    {
        private readonly Func<IDataContainer> _dataContainer;

        public HourService(Func<IDataContainer> dataContainer)
        {
            _dataContainer = dataContainer;
        }

        public async Task<IEnumerable<HourBl>> GetAllHoursAsync()
        {
            using (var dataContainer = _dataContainer())
            {
                var hoursBl = (await dataContainer.Hours.ToListAsync()).Select(h => AutoMapper.Mapper.Map<HourD, HourBl>(h));
                return hoursBl;
            }
        }

        public async Task<HourBl> GetHourByIdAsync(int id)
        {
            {
                var hours = await GetAllHoursAsync();
                return hours.SingleOrDefault(x => x.Id == id);
            }
        }
    }
}
