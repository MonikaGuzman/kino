﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business.Services
{
    public class MailSendingService : IMailSendingService
    {
        public void MailSend(OrderBl order, List<TicketBl> tickets)
        {
            string movieTitle = "";
            DateTime dateTime = new DateTime();
            TimeSpan hour = new TimeSpan();
            string cinemaName = "";
            string cinemaAddress = "";

            foreach (var ticket in tickets)
            {
                movieTitle = ticket.Film.Title;
                dateTime = ticket.DateTime;
                hour = ticket.Hour.Hour;
                cinemaName = ticket.Cinema.Name;
                cinemaAddress = ticket.Cinema.Adress;
            }

            string sender = GetMailSender(tickets);
            string receiver = GetMailReceiver(tickets);

            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");

            mail.From = new MailAddress(sender);
            mail.To.Add(receiver);
            mail.Subject = "Mail with your order";
            mail.Body = $"Movie title: {movieTitle}"
            + $"\n data i godzina: {dateTime} {hour}"
            + $"\n Nazwa i adres kina: {cinemaName} {cinemaAddress}"
            + $"\n Ilosc biletow: {order.AmountOfTheTicket}"
            + $"\n Cena: {order.TotalPrice}";

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential("username", "password");
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
        }

        public string GetMailSender(List<TicketBl> tickets)
        {
            string mail = "";
            foreach (var ticket in tickets)
            {
                return ticket.Cinema.Mail;
            }

            return mail;
        }

        public string GetMailReceiver(List<TicketBl> tickets)
        {
            string mail = "";

            foreach (var ticket in tickets)
            {
                return ticket.Client.MailAddress;
            }

            return mail;
        }
    }
}
