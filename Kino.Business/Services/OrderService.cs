﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using Kino.Data.Interfaces;
using Kino.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business.Services
{
    public class OrderService : IOrderService
    {
        private readonly Func<IDataContainer> _dataContainer;
        private IMailSendingService _mailSendingService;
        private ITicketService _ticketService;

        public OrderService(Func<IDataContainer> dataContainer, IMailSendingService mailSendingService, ITicketService ticketService)
        {
            _dataContainer = dataContainer;
            _mailSendingService = mailSendingService;
            _ticketService = ticketService;
        }

        public double CalculateOrder(List<TicketBl> tickets)
        {
            List<double> price = new List<double>();

            foreach (var ticket in tickets)
            {
                price.Add(ticket.Price);
            }

            var totalPrice = price.Sum();
            return totalPrice;
        }

        public int AmountOfTheTickets(List<TicketBl> tickets)
        {
            int howManyTicketsWasBought = tickets.Count();

            return howManyTicketsWasBought;
        }

        public async Task OrderAddAsync(OrderBl order, List<TicketBl> tickets)
        {
            using (var dataContainer = _dataContainer())
            {
                var orderToAdd = AutoMapper.Mapper.Map<OrderBl, Order>(order);
                foreach (var ticketId in order.TicketsId)
                {
                    orderToAdd.Tickets.Add(await dataContainer.Tickets.FindAsync(ticketId));
                }

                dataContainer.Orders.Add(orderToAdd);
                await dataContainer.SaveChangesRepoAsync();
            }

            _mailSendingService.MailSend(order, tickets);
        }
    }
}
