﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using Kino.Data.Interfaces;
using Kino.Data.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Kino.Business.Services
{
    public class ProgrammeService : IProgrammeService
    {
        public static List<ProgrammeBl> _programmes = new List<ProgrammeBl>();

        private readonly Func<IDataContainer> _dataContainer;
        private IFilmService _filmService;
        private ICinemaService _cinemaService;
        private IScreeningRoomService _screeningRoomService;

        public ProgrammeService(Func<IDataContainer> dataContainer, IFilmService filmService, ICinemaService cinemaService, IScreeningRoomService screeningRoomService)
        {
            _dataContainer = dataContainer;
            _filmService = filmService;
            _cinemaService = cinemaService;
            _screeningRoomService = screeningRoomService;
        }

        public async Task<bool> DoesMovieWasPlayedBeforeInCinemaAsync(ProgrammeBl programme)
        {
            using (var dataContainer = _dataContainer())
            {
                bool doesMovieWasPlayedBeforeInCinema = await dataContainer.Programmes.AnyAsync(p => p.Film.Id == programme.Film.Id && p.Cinema.Id == programme.Cinema.Id);
                return doesMovieWasPlayedBeforeInCinema;
            }
        }

        public async Task<IEnumerable<ProgrammeBl>> GetAllProgrammesAsync()
        {
            using (var dataContainer = _dataContainer())
            {
                var programmesBl = (await dataContainer.Programmes.Include(x => x.Film)
                    .Include(x => x.Hours.Select(y => y.ScreeningRoom)).ToListAsync())
                    .Select(p => AutoMapper.Mapper.Map<Programme, ProgrammeBl>(p));
                return programmesBl;
            }
        }

        public async Task<DateTime> LastDataOfPlayedMovieAsync(ProgrammeBl programme)
        {
            using (var dataContainer = _dataContainer())
            {
                var endDataOfLastProgramme = DateTime.UtcNow;

                var doesTheSameTitleOfMovieExist = (await dataContainer.Films.AnyAsync(f => f.Title == programme.Film.Title));

                if (doesTheSameTitleOfMovieExist)
                {
                    endDataOfLastProgramme = (await dataContainer.Programmes.Where(p => p.Cinema.Name == programme.Cinema.Name
                    && p.Film.Title == programme.Film.Title).OrderByDescending(x => x.EndDate).FirstAsync()).EndDate;

                    return endDataOfLastProgramme;
                }

                return endDataOfLastProgramme;
            }
        }

        public async Task AddProgrammeAsync(ProgrammeBl programme)
        {
            using (var dataContainer = _dataContainer())
            {
                var programmeToAdd = AutoMapper.Mapper.Map<ProgrammeBl, Programme>(programme);

                programmeToAdd.Cinema = dataContainer.Cinemas.First(x => x.Name == programme.Cinema.Name);

                foreach (var item in programmeToAdd.Hours)
                {
                    item.ScreeningRoom = dataContainer.ScreeningRooms.FirstOrDefault(x => x.Id == item.ScreeningRoom.Id);
                }

                dataContainer.Films.Attach(programmeToAdd.Film);
                dataContainer.Programmes.Add(programmeToAdd);
                await dataContainer.SaveChangesRepoAsync();
            }
        }

        public async Task DetermineDateForProgrammeAsync(ProgrammeBl programme)
        {
            var firstDayOfTheNextWeek = (await LastDataOfPlayedMovieAsync(programme)).AddDays(1);
            var lastDayOfTheNextWeek = firstDayOfTheNextWeek.AddDays(6);

            programme.StartDate = firstDayOfTheNextWeek;
            programme.EndDate = lastDayOfTheNextWeek;
        }

        public void DetermineDateForProgramme(ProgrammeBl programme, DateTime startDate)
        {
            if (startDate.DayOfWeek.ToString() != "Friday")
            {
                throw new Exception("Tydzien zaczyna sie w piatek. Musisz podac date piatku");
            }
            else
            {
                DateTime endDate = startDate.AddDays(6);

                programme.StartDate = startDate;
                programme.EndDate = endDate;
            }
        }

        public void DoesMovieIsPlayedBeforeCinemaOpening(ProgrammeBl programme)
        {
            var openingCinemaHour = programme.Cinema.OpenHour;
            var seanceHours = programme.Hours.Select(x => x.Hour);

            foreach (var seanceHour in seanceHours)
            {
                if (seanceHour < openingCinemaHour)
                {
                    throw new Exception("Film nie moze sie zaczynac przed otwarciem kina!");
                }
            }
        }

        public async Task DoesMoviesTieInWithAsync(ProgrammeBl programme, HourBl hour, ScreeningRoomBl screeningRoom)
        {
            var lengthOfTheSelectedMovie = programme.Film.LengthOfTheFilm;
            var lengthOfTheMovieInTimeSpan = TimeSpan.FromMinutes(lengthOfTheSelectedMovie);

            var startingMovieHour = hour.Hour;
            var endingMovieHour = startingMovieHour.Add(lengthOfTheMovieInTimeSpan);

            var breakBetweenMovies = TimeSpan.FromMinutes(10);

            var programmesForCinema = await _cinemaService.GetProgrammeForSelectedCinemaAndForDataAsync(programme);

            foreach (var program in programmesForCinema)
            {
                foreach (var hourItem in program.Hours)
                {
                    if (hourItem.ScreeningRoom.RoomNumber == screeningRoom.RoomNumber)
                    {
                        var start = hourItem.Hour;

                        var lengthOfTheMovie = program.Film.LengthOfTheFilm;
                        var lengthInCorrectFormat = TimeSpan.FromMinutes(lengthOfTheMovie);

                        var end = hourItem.Hour.Add(lengthInCorrectFormat);

                        var condition1 = start <= startingMovieHour;
                        var condition2 = startingMovieHour <= end;
                        var condition3 = start <= endingMovieHour;
                        var condition4 = endingMovieHour <= end;

                        if ((condition1 && condition2) || (condition3 && condition4))
                        {
                            throw new Exception("W tym czasie, w tej sali juz jest zaplanowany inny seans");
                        }

                        if ((end >= (startingMovieHour - breakBetweenMovies)) || ((endingMovieHour + breakBetweenMovies) <= start))
                        {
                            throw new Exception("Przerwa miedzy seansami wynosi mniej niz 10 min");
                        }
                    }
                }
            }
        }

        public void DoesThePathIsCorrect(string path)
        {
            var regex = new Regex(@"^(?:[a-zA-Z]\:|\\\\[\w\.]+\\[\w.$]+)\\(?:[\w]+\\)*\w([\w.])+$");

            var match = regex.Match(path);

            if (!match.Success)
            {
                throw new ArgumentException("Podana sciezka nie jest prawidlowa");
            }
        }

        public void ImportProgrammeToJson(List<ProgrammeBl> programmes, string path)
        {
            var fileName = @"Repertuar.json";
            var dataToSave = JsonConvert.SerializeObject(programmes, Formatting.Indented);
            File.WriteAllText(Path.Combine(path, fileName), dataToSave);
        }

        public void ImportProgrammeToXml(List<ProgrammeBl> programmes, string path)
        {
            var fileName = @"Repertuar.xml";
            XmlSerializer serializer = new XmlSerializer(typeof(List<ProgrammeBl>), new XmlRootAttribute("ProgrammeBl"));
            StringBuilder data = new StringBuilder();
            serializer.Serialize(new StringWriter(data), programmes);
            File.WriteAllText(path + fileName, data.ToString());
        }

        public async Task<bool> DoesExistedCinemaWasChosenAsync(CinemaBl cinemaName)
        {
            var programmes = await GetAllProgrammesAsync();
            bool isCorrect = false;

            var doesTheSameCinemaWasChosen = programmes.Any(p => p.Cinema.Name == cinemaName.Name);

            if (doesTheSameCinemaWasChosen)
            {
                return isCorrect = true;
            }

            if (!doesTheSameCinemaWasChosen)
            {
                throw new ArgumentException("Nie ma takiego kina!");
            }

            return isCorrect;
        }

        public async Task<List<ProgrammeBl>> GetProgrammeByCinemaNameAndDateAsync(CinemaBl cinemaName, DateTime data)
        {
            using (var dataContainer = _dataContainer())
            {
                List<ProgrammeBl> _programmes = new List<ProgrammeBl>();

                var doesTheSameCinemaWasChosen = (await dataContainer.Cinemas.AnyAsync(x => x.Name == cinemaName.Name));

                if (doesTheSameCinemaWasChosen)
                {
                    var seletedProgrammes = (await dataContainer.Programmes.Include(x => x.Film).Include(x => x.Hours.Select(y => y.ScreeningRoom))
                        .Where(x => x.Cinema.Name == cinemaName.Name).ToListAsync())
                    .Select(p => AutoMapper.Mapper.Map<Programme, ProgrammeBl>(p));

                    foreach (var programmeItem in seletedProgrammes)
                    {
                        var resultStart = DateTime.Compare(data, programmeItem.StartDate);
                        var resultEnd = DateTime.Compare(data, programmeItem.EndDate);

                        if (resultStart >= 0 && resultEnd <= 0)
                        {
                            _programmes.Add(programmeItem);
                        }
                    }

                    if (_programmes.Count == 0)
                    {
                        throw new Exception("Dla wybranego kina nie ma repertuaru na podany przez Ciebie tydzień");
                    }

                    return _programmes;
                }

                if (!doesTheSameCinemaWasChosen)
                {
                    throw new ArgumentException("Nie ma takiego kina!");
                }

                return _programmes;
            }
        }

        public async Task<CinemaBl> GetCinemaByIdAsync(int id)
        {
            var cinemas = await _cinemaService.GetAllCinemasAsync();

            return cinemas.SingleOrDefault(x => x.Id == id);
        }

        public async Task<FilmBl> GetMovieByIdAsync(int id)
        {
            var movies = await _filmService.GetAllMoviesAsync();

            return movies.SingleOrDefault(x => x.Id == id);
        }

        public async Task<ScreeningRoomBl> GetScreeningRoomsByIdAsync(int cinemaId, int roomId)
        {
            var screeningRoom = new ScreeningRoomBl();

            var screeningRooms = await _screeningRoomService.GetAllScreeningRoomsAsync();
            var cinemas = await _cinemaService.GetAllCinemasAsync();

            foreach (var cinema in cinemas)
            {
                if (cinema.Id == cinemaId)
                {
                    foreach (var room in screeningRooms)
                    {
                        if (room.Id == roomId)
                        {
                            return screeningRoom = room;
                        }
                    }
                }
            }

            return screeningRoom;
        }

        public async Task<CinemaBl> GetCinemaByNameAsync(string name)
        {
            var cinemas = await _cinemaService.GetAllCinemasAsync();

            return cinemas.SingleOrDefault(c => c.Name == name);
        }

        public async Task<FilmBl> GetMovieByTitleAsync(string title)
        {
            var movies = await _filmService.GetAllMoviesAsync();

            return movies.SingleOrDefault(m => m.Title == title);
        }

        public async Task<ScreeningRoomBl> ChooseScreeningRoomsByNumberAsync(string cinemaName, int roomNumber)
        {
            var screeningRoom = new ScreeningRoomBl();

            var screeningRooms = await _screeningRoomService.GetAllScreeningRoomsAsync();
            var cinemas = await _cinemaService.GetAllCinemasAsync();

            foreach (var cinema in cinemas)
            {
                if (cinema.Name == cinemaName)
                {
                    foreach (var room in screeningRooms)
                    {
                        if (room.RoomNumber == roomNumber)
                        {
                            return screeningRoom = room;
                        }
                    }
                }
            }

            return screeningRoom;
        }
    }
}
