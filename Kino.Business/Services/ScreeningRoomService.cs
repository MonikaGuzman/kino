﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using Kino.Data;
using Kino.Data.Interfaces;
using Kino.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business
{
    public class ScreeningRoomService : IScreeningRoomService
    {
        private readonly Func<IDataContainer> _dataContainer;

        public ScreeningRoomService(Func<IDataContainer> dataContainer)
        {
            _dataContainer = dataContainer;
        }

        public async Task<IEnumerable<ScreeningRoomBl>> GetAllScreeningRoomsAsync()
        {
            using (var dataContainer = _dataContainer())
            {
                var screeningRoomsBl = (await dataContainer.ScreeningRooms.ToListAsync())
                    .Select(c => AutoMapper.Mapper.Map<ScreeningRoom, ScreeningRoomBl>(c));
                return screeningRoomsBl;
            }
        }

        public bool IsNegativeValueOfScreeningRoom(ScreeningRoomBl screeningRoom)
        {
            bool isNegative = false;

            if (screeningRoom.RoomNumber < 0)
            {
                return isNegative = true;
            }

            return isNegative;
        }

        public bool IsMoreRowThanLatinLetters(int amountOfRow)
        {
            if (amountOfRow > 26)
            {
                throw new Exception("There are more rows than in the Latin alphabet");
            }

            return true;
        }

        public int LetterToInt(string letter)
        {
            char c = letter[0];
            for (int i = 0; i < letter.Length; i++)
            {
                c = (char)letter[i];
                return char.ToUpper(c) - 64;
            }
            return c;
        }

        public string IntToLetters(int value)
        {
            string result = string.Empty;
            while (--value >= 0)
            {
                result = (char)('A' + value % 26) + result;
                value /= 26;
            }
            return result;
        }

        public ScreeningRoomBl GetScreeningRoomForProgrammeAndHour(List<ProgrammeBl> programmes, TimeSpan hour)
        {
            var screeningRoom = new ScreeningRoomBl();
            var hoursForProgramme = programmes.Select(x => x.Hours);

            foreach (var hourList in hoursForProgramme)
            {
                foreach (var hourSingle in hourList)
                {
                    if (hourSingle.Hour == hour)
                    {
                        screeningRoom = hourSingle.ScreeningRoom;
                        return screeningRoom;
                    }
                }
            }

            return screeningRoom;
        }

        public string GetLetterOfTheLastRow(ScreeningRoomBl screeningRoom)
        {
            int amountOfRow = screeningRoom.AmountOfRow;
            var letterOfLastRow = IntToLetters(amountOfRow);

            return letterOfLastRow;
        }
    }
}
