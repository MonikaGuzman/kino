﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using Kino.Data.Interfaces;
using Kino.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Business.Services
{
    public class TicketService : ITicketService
    {
        private readonly Func<IDataContainer> _dataContainer;
        private IScreeningRoomService _screeningRoomService;
        private IClientService _clientService;

        public TicketService(Func<IDataContainer> dataContainer, IScreeningRoomService screeningRoomService, IClientService clientService)
        {
            _dataContainer = dataContainer;
            _screeningRoomService = screeningRoomService;
            _clientService = clientService;
        }

        public async Task<int> AddTicketAsync(TicketBl ticket)
        {
            await DoesPlaceIsFreeAsync(ticket);
            var ticketToAdd = AutoMapper.Mapper.Map<TicketBl, Ticket>(ticket);

            using (var dataContainer = _dataContainer())
            {
                ticketToAdd.Cinema = dataContainer.Cinemas.First(x => x.Name == ticket.Cinema.Name);
                dataContainer.Films.Attach(ticketToAdd.Film);
                dataContainer.Clients.Attach(ticketToAdd.Client);

                dataContainer.Hours.Attach(ticketToAdd.Hour);
                dataContainer.Tickets.Add(ticketToAdd);
                await dataContainer.SaveChangesRepoAsync();
            }

            ticket.Id = ticketToAdd.Id;
            return ticket.Id;
        }

        public double CalculatePriceOfTheTicketByDay(DateTime date)
        {
            double price = 0;
            var day = date.DayOfWeek;

            if ((day >= DayOfWeek.Monday) && (day <= DayOfWeek.Thursday))
            {
                return price = 20;
            }

            if ((day == DayOfWeek.Friday) || (day == DayOfWeek.Sunday) || (day == DayOfWeek.Saturday))
            {
                return price = 25;
            }

            return price;
        }

        public async Task<double> CalculatePriceWithPropmotion(DateTime dateTime, string mail)
        {
            var price = CalculatePriceOfTheTicketByDay(dateTime);
            var tickets = await _clientService.GetAllTicketsForClientAsync(mail);
            var count = 1;

            int howManyTicketsTheClientBoth = tickets.Count();
            int numberOfCurrentTicket = howManyTicketsTheClientBoth + 1;

            if (numberOfCurrentTicket % 10 == 0)
            {
                return price = 0;
            }

            foreach (var ticket in tickets)
            {
                var month = ticket.DateTime.Month;
                if (month == dateTime.Month)
                {
                    count++;
                }
            }

            var currentCount = count + 1;

            if (currentCount >= 5)
            {
                return price = price * 0.8;
            }

            return price;
        }

        public async Task DoesPlaceIsFreeAsync(TicketBl ticket)
        {
            using (var dataContainer = _dataContainer())
            {
                var doesSeatIsNotAvailable = await dataContainer.Tickets.AnyAsync(x => x.Cinema.Name == ticket.Cinema.Name && x.DateTime == ticket.DateTime
                && x.Hour.Id == ticket.Hour.Id && x.Row == ticket.Row && x.Seat == ticket.Seat);

                if (doesSeatIsNotAvailable)
                {
                    throw new Exception("This seat is not available");
                }
            }
        }

        public void IsLetter(string checkIsLetter)
        {
            var letter = Convert.ToChar(checkIsLetter);
            var isLetter = char.IsLetter(letter);

            if (!isLetter)
            {
                throw new Exception($"{checkIsLetter} is not a letter");
            }
        }

        public void IsRowExist(ScreeningRoomBl screeningRoom, string row)
        {
            int rowNumber = _screeningRoomService.LetterToInt(row);

            if (rowNumber > screeningRoom.AmountOfRow || rowNumber <= 0)
            {
                throw new Exception("There is not such row in this screening room");
            }
        }

        public void IsSeatExist(ScreeningRoomBl screeningRoom, int seat)
        {
            if (seat > screeningRoom.SeatsInRow)
            {
                throw new Exception("There are not so much seats in the row");
            }

            if (seat <= 0)
            {
                throw new Exception($"{seat} cannot be less then or equal 0");
            }
        }

        public List<TicketBl> AddTicketToList(TicketBl ticket)
        {
            List<TicketBl> tickets = new List<TicketBl>();
            tickets.Add(ticket);

            return tickets;
        }

        public void CheckIfTicketHasCorrectData(List<TicketBl> tickets)
        {
            for (int i = 0; i < tickets.Count(); i++)
            {
                for (int j = i + 1; j < tickets.Count(); j++)
                {
                    if (tickets[i].Client.MailAddress != tickets[j].Client.MailAddress)
                    {
                        throw new Exception("Client e-mail address is not the same!");
                    }

                    if (tickets[i].Cinema.Id != tickets[j].Cinema.Id)
                    {
                        throw new Exception("Cinema is not the same!");
                    }

                    if (tickets[i].Film.Id != tickets[j].Film.Id )
                    {
                        throw new Exception("Movie is not the same!");
                    }

                    if (tickets[i].Hour.Id != tickets[j].Hour.Id)
                    {
                        throw new Exception("Hour is not the same!");
                    }
                }
            }
        }

        public void CheckIfUserHasAllNecessaryProperties(List<TicketBl> tickets)
        {
            foreach (var ticket in tickets)
            {
                if (ticket.Client == null)
                {
                    throw new Exception("Client cannot be null");
                }

                if (ticket.Cinema == null)
                {
                    throw new Exception("Cinema cannot be null");
                }

                if (ticket.DateTime == null)
                {
                    throw new Exception("Date cannot be null");
                }

                if (ticket.Film == null)
                {
                    throw new Exception("Movie cannot be null");
                }

                if (ticket.Hour == null)
                {
                    throw new Exception("Hour cannot be null");
                }

                if (ticket.Row == null)
                {
                    throw new Exception("Row cannot be null");
                }
            }
        }
    }
}
