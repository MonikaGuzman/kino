﻿using Kino.Data.Interfaces;
using Kino.Data.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data
{
    public class DataContainer : DbContext, IDataContainer
    {
        public DataContainer() : base("CinemaDBConnectionString")
        {

        }

        public DbSet<Film> Films { get; set; }
        public DbSet<Cinema> Cinemas { get; set; }
        public DbSet<ScreeningRoom> ScreeningRooms { get; set; }
        public DbSet<Programme> Programmes { get; set; }
        public DbSet<HourD> Hours { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Order> Orders { get; set; }
        public Task<int> SaveChangesRepoAsync() { return this.SaveChangesAsync(); }
    }
}
