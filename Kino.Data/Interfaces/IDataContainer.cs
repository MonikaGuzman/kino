﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Kino.Data.Models;

namespace Kino.Data.Interfaces
{
    public interface IDataContainer : IDisposable
    {
        DbSet<Cinema> Cinemas { get; set; }
        DbSet<Film> Films { get; set; }
        DbSet<ScreeningRoom> ScreeningRooms { get; set; }
        DbSet<Programme> Programmes { get; set; }
        DbSet<HourD> Hours { get; set; }
        DbSet<Client> Clients { get; set; }
        DbSet<Ticket> Tickets { get; set; }
        DbSet<Order> Orders { get; set; }
        Task<int> SaveChangesRepoAsync();
    }
}