namespace Kino.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cinemas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Adress = c.String(),
                        Mail = c.String(),
                        Telephone = c.Int(nullable: false),
                        OpenHour = c.Time(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Programmes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Cinema_Id = c.Int(),
                        Film_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cinemas", t => t.Cinema_Id)
                .ForeignKey("dbo.Films", t => t.Film_Id)
                .Index(t => t.Cinema_Id)
                .Index(t => t.Film_Id);
            
            CreateTable(
                "dbo.Films",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        FilmGenre = c.Int(nullable: false),
                        LengthOfTheFilm = c.Int(nullable: false),
                        Description = c.String(),
                        Website = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.HourDs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Hour = c.Time(nullable: false, precision: 7),
                        ScreeningRoom_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ScreeningRooms", t => t.ScreeningRoom_Id)
                .Index(t => t.ScreeningRoom_Id);
            
            CreateTable(
                "dbo.ScreeningRooms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoomNumber = c.Int(nullable: false),
                        AmountOfRow = c.Int(nullable: false),
                        SeatsInRow = c.Int(nullable: false),
                        Cinema_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cinemas", t => t.Cinema_Id)
                .Index(t => t.Cinema_Id);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                        MailAddress = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Price = c.Double(nullable: false),
                        Row = c.String(),
                        Seat = c.Int(nullable: false),
                        Cinema_Id = c.Int(),
                        Client_Id = c.Int(),
                        Film_Id = c.Int(),
                        Hour_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cinemas", t => t.Cinema_Id)
                .ForeignKey("dbo.Clients", t => t.Client_Id)
                .ForeignKey("dbo.Films", t => t.Film_Id)
                .ForeignKey("dbo.HourDs", t => t.Hour_Id)
                .Index(t => t.Cinema_Id)
                .Index(t => t.Client_Id)
                .Index(t => t.Film_Id)
                .Index(t => t.Hour_Id);
            
            CreateTable(
                "dbo.HourDProgrammes",
                c => new
                    {
                        HourD_Id = c.Int(nullable: false),
                        Programme_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.HourD_Id, t.Programme_Id })
                .ForeignKey("dbo.HourDs", t => t.HourD_Id, cascadeDelete: true)
                .ForeignKey("dbo.Programmes", t => t.Programme_Id, cascadeDelete: true)
                .Index(t => t.HourD_Id)
                .Index(t => t.Programme_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tickets", "Hour_Id", "dbo.HourDs");
            DropForeignKey("dbo.Tickets", "Film_Id", "dbo.Films");
            DropForeignKey("dbo.Tickets", "Client_Id", "dbo.Clients");
            DropForeignKey("dbo.Tickets", "Cinema_Id", "dbo.Cinemas");
            DropForeignKey("dbo.HourDs", "ScreeningRoom_Id", "dbo.ScreeningRooms");
            DropForeignKey("dbo.ScreeningRooms", "Cinema_Id", "dbo.Cinemas");
            DropForeignKey("dbo.HourDProgrammes", "Programme_Id", "dbo.Programmes");
            DropForeignKey("dbo.HourDProgrammes", "HourD_Id", "dbo.HourDs");
            DropForeignKey("dbo.Programmes", "Film_Id", "dbo.Films");
            DropForeignKey("dbo.Programmes", "Cinema_Id", "dbo.Cinemas");
            DropIndex("dbo.HourDProgrammes", new[] { "Programme_Id" });
            DropIndex("dbo.HourDProgrammes", new[] { "HourD_Id" });
            DropIndex("dbo.Tickets", new[] { "Hour_Id" });
            DropIndex("dbo.Tickets", new[] { "Film_Id" });
            DropIndex("dbo.Tickets", new[] { "Client_Id" });
            DropIndex("dbo.Tickets", new[] { "Cinema_Id" });
            DropIndex("dbo.ScreeningRooms", new[] { "Cinema_Id" });
            DropIndex("dbo.HourDs", new[] { "ScreeningRoom_Id" });
            DropIndex("dbo.Programmes", new[] { "Film_Id" });
            DropIndex("dbo.Programmes", new[] { "Cinema_Id" });
            DropTable("dbo.HourDProgrammes");
            DropTable("dbo.Tickets");
            DropTable("dbo.Clients");
            DropTable("dbo.ScreeningRooms");
            DropTable("dbo.HourDs");
            DropTable("dbo.Films");
            DropTable("dbo.Programmes");
            DropTable("dbo.Cinemas");
        }
    }
}
