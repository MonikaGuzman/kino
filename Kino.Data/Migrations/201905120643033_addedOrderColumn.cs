namespace Kino.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedOrderColumn : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TotalPrice = c.Double(nullable: false),
                        AmountOfTheTicket = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Tickets", "Order_Id", c => c.Int());
            CreateIndex("dbo.Tickets", "Order_Id");
            AddForeignKey("dbo.Tickets", "Order_Id", "dbo.Orders", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tickets", "Order_Id", "dbo.Orders");
            DropIndex("dbo.Tickets", new[] { "Order_Id" });
            DropColumn("dbo.Tickets", "Order_Id");
            DropTable("dbo.Orders");
        }
    }
}
