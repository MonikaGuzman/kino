namespace Kino.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedOrderColumn1 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Tickets", new[] { "Order_Id" });
            CreateIndex("dbo.Tickets", "order_Id");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tickets", new[] { "order_Id" });
            CreateIndex("dbo.Tickets", "Order_Id");
        }
    }
}
