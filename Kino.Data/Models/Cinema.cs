﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data.Models
{
    public class Cinema
    {
        public Cinema()
        {
            Programmes = new HashSet<Programme>();
            ScreeningRooms = new List<ScreeningRoom>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
        public string Mail { get; set; }
        public int Telephone { get; set; }
        public TimeSpan OpenHour { get; set; }
        public List<ScreeningRoom> ScreeningRooms { get; set; }

        public ICollection<Programme> Programmes { get; set; }
    }
}
