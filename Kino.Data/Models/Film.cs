﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data.Models
{
    public class Film
    {
        public enum Genre
        {
            Comedy,
            Drama,
            Horror,
            Fantasy
        }

        public Film()
        {
            Programmes = new HashSet<Programme>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public Genre FilmGenre { get; set; }
        public int LengthOfTheFilm { get; set; }
        public string Description { get; set; }
        public string Website { get; set; }

        public ICollection<Programme> Programmes { get; set; }
    }
}
