﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data.Models
{
    public class HourD
    {
        public HourD()
        {
            Programmes = new HashSet<Programme>();
        }

        public int Id { get; set; }
        public TimeSpan Hour { get; set; }
        public ScreeningRoom ScreeningRoom { get; set; }

        public ICollection<Programme> Programmes { get; set; }
    }
}
