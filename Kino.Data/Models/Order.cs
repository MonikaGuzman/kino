﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data.Models
{
    public class Order
    {
        public int Id { get; set; }
        public double TotalPrice { get; set; }
        public int AmountOfTheTicket { get; set; }
        public ICollection<Ticket> Tickets { get; set; }
    }
}
