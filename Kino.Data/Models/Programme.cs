﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data.Models
{
    public class Programme
    {
        public Programme()
        {
            Hours = new List<HourD>();
        }

        public int Id { get; set; }
        public Cinema Cinema { get; set; }
        public Film Film { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<HourD> Hours { get; set; }        
    }
}
