﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data.Models
{
    public class ScreeningRoom
    {
        public int Id { get; set; }
        public int RoomNumber { get; set; }
        public int AmountOfRow { get; set; }
        public int SeatsInRow { get; set; }
        public Cinema Cinema { get; set; }

        public List<HourD> Hours { get; set; }
    }
}
