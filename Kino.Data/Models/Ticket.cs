﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public double Price { get; set; }
        public Cinema Cinema { get; set; }
        public Film Film { get; set; }
        public DateTime DateTime { get; set; }
        public HourD Hour { get; set; }
        public string Row { get; set; }
        public int Seat { get; set; }
        public Client Client { get; set; }
    }
}
