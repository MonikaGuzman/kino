﻿using Kino.Data.Interfaces;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Data
{
    public class ModuleData : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IDataContainer>().ToMethod(x => new DataContainer());
        }
    }
}
