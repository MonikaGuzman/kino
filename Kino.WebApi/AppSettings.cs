﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.WebApi
{
    internal interface IAppSettings
    {
        string WebApiUri { get; }
    }

    internal class AppSettings : IAppSettings
    {
        public string WebApiUri => ConfigurationManager.AppSettings["WebApiUri"];
    }
}
