﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Kino.WebApi.Controllers
{
    public class CinemaController : ApiController
    {
        private ICinemaService _cinemaService;

        public CinemaController(ICinemaService cinemaService)
        {
            _cinemaService = cinemaService;
        }

        public async Task Post(CinemaBl cinema)
        {
            if (cinema == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (!ModelState.IsValid)
            {
                Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            try
            {
                await _cinemaService.AddCinemaAsync(cinema);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
