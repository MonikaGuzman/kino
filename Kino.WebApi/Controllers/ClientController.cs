﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Kino.WebApi.Controllers
{
    public class ClientController : ApiController
    {
        private IClientService _clientService;

        public ClientController(IClientService clientService)
        {
            _clientService = clientService;
        }

        public async Task Post(ClientBl client)
        {
            try
            {
                await _clientService.AddClient(client);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
