﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Kino.WebApi.Controllers
{
    public class FilmController : ApiController
    {
        private IFilmService _filmService;

        public FilmController(IFilmService filmService)
        {
            _filmService = filmService;
        }

        public async Task Post(FilmBl film)
        {
            if (film == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            if (!ModelState.IsValid)
            {
                Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            try
            {
                await _filmService.AddFilmAsync(film);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
