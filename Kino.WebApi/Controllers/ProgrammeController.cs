﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Kino.WebApi.Controllers
{
    public class ProgrammeController : ApiController
    {
        private IProgrammeService _programmeService;

        public ProgrammeController(IProgrammeService programmeService)
        {
            _programmeService = programmeService;
        }

        // api/programme?cinemaName=helios&&dateTime=04/13/2019
        public async Task<List<ProgrammeBl>> GetProgramme(string cinemaName, DateTime dateTime)
        {
            var cinema = new CinemaBl() { Name = cinemaName };
            var programme = new List<ProgrammeBl>();

            try
            {
                return await _programmeService.GetProgrammeByCinemaNameAndDateAsync(cinema, dateTime);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return programme;
        }

        //api/programme?cinemaId=1&&movieId=1&&screeningRoomId=1&&hour=16:00&&04/26/2019
        public async Task Post(int cinemaId, int movieId, int screeningRoomId, TimeSpan hour, DateTime startDate)
        {
            List<ScreeningRoomBl> screeningRooms = new List<ScreeningRoomBl>();
            var programme = new ProgrammeBl();

            programme.Cinema = await _programmeService.GetCinemaByIdAsync(cinemaId);
            programme.Film = await _programmeService.GetMovieByIdAsync(movieId);
            var screeningRoom = await _programmeService.GetScreeningRoomsByIdAsync(cinemaId, screeningRoomId);

            var Hour = new HourBl() { Hour = hour, ScreeningRoom = screeningRoom };
            List<HourBl> hourBls = new List<HourBl>();
            hourBls.Add(Hour);

            programme.Hours = hourBls;

            bool doesMovieWasPlayedBeforeInCinema = await _programmeService.DoesMovieWasPlayedBeforeInCinemaAsync(programme);
            try
            {
                if (doesMovieWasPlayedBeforeInCinema)
                {
                    await _programmeService.DetermineDateForProgrammeAsync(programme);
                }

                if (!doesMovieWasPlayedBeforeInCinema)
                {
                    _programmeService.DetermineDateForProgramme(programme, startDate);
                }

                await _programmeService.AddProgrammeAsync(programme);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }
    }
}
