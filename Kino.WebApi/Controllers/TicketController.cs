﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Kino.WebApi.Controllers
{
    public class TicketController : ApiController
    {
        private ITicketService _ticketService;
        private IClientService _clientService;
        private IOrderService _orderService;

        public TicketController(ITicketService ticketService, IClientService clientService, IOrderService orderService)
        {
            _ticketService = ticketService;
            _clientService = clientService;
            _orderService = orderService;
        }

        public async Task<IEnumerable<TicketBl>> Get(string mail)
        {
            return await _clientService.GetAllTicketsForClientAsync(mail);
        }

        public async Task<IHttpActionResult> Post(List<TicketBl> tickets)
        {
            var order = new OrderBl();

            try
            {
                _ticketService.CheckIfTicketHasCorrectData(tickets);
                _ticketService.CheckIfUserHasAllNecessaryProperties(tickets);

                foreach (var ticket in tickets)
                {
                    _ticketService.IsLetter(ticket.Row);
                    _ticketService.IsRowExist(ticket.Hour.ScreeningRoom, ticket.Row);
                    _ticketService.IsSeatExist(ticket.Hour.ScreeningRoom, ticket.Seat);
                    await _clientService.DoesEmailExistAsync(ticket.Client.MailAddress);
                    order.AmountOfTheTicket += 1;
                    ticket.Price = await _ticketService.CalculatePriceWithPropmotion(ticket.DateTime, ticket.Client.MailAddress);
                    order.TotalPrice += ticket.Price;
                    int ticketId = await _ticketService.AddTicketAsync(ticket);
                    order.TicketsId.Add(ticketId);
                }
                await _orderService.OrderAddAsync(order, tickets);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
