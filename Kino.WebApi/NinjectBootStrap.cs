﻿using Kino.Business;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.WebApi
{
    public class NinjectBootStrap
    {
        public IKernel GetKernel()
        {
            var kernel = new StandardKernel(new ModuleBusiness());
            return kernel;
        }
    }
}
