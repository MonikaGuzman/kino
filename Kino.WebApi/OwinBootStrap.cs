﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.WebApi
{
    internal class OwinBootStrap
    {
        private IAppSettings _appSettings = new AppSettings();
        private IDisposable _webApi;

        public void Start()
        {
            var baseAddress = _appSettings.WebApiUri;
            _webApi = WebApp.Start<Startup>(url: baseAddress);
        }

        public void Stop()
        {
            _webApi.Dispose();
        }
    }
}
