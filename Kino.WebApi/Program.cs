﻿using Kino.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;

namespace Kino.WebApi
{
    public class Program
    {

        static void Main(string[] args)
        {
            MapperConfig.CreateMap();

            var rc = HostFactory.Run(x =>
            {
                x.Service<OwinBootStrap>(s =>
                {
                    s.ConstructUsing(name => new OwinBootStrap());
                    s.WhenStarted(owinBootStrap => owinBootStrap.Start());
                    s.WhenStopped(owinBootStrap => owinBootStrap.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("Kino server WebApi Service");
                x.SetDisplayName("KinoWebApi");
                x.SetServiceName("KinoWebApi");
            });

            var exitCode = (int)Convert.ChangeType(rc, rc.GetTypeCode());
            Environment.ExitCode = exitCode;
        }
    }
}
