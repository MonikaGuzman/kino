﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Helpers
{
    public class ConsoleUtilities
    {
        public static string GetStringFromConsole(string message)
        {
            Console.WriteLine(message);
            string returnMessage = Console.ReadLine();

            while (string.IsNullOrWhiteSpace(returnMessage))
            {
                Console.WriteLine("Dodales puste pole");
                Console.WriteLine(message);
                returnMessage = Console.ReadLine();
            }

            return returnMessage;
        }

        public static int GetIntFromConsole(string message)
        {
            int number;

            while (!int.TryParse(GetStringFromConsole(message), out number))
            {
                Console.WriteLine("Nie wpisales liczby");
            }

            return number;
        }
    }
}
