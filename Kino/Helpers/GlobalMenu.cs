﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using Kino.Business.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Helpers
{
    public class GlobalMenu
    {
        IFilmService _filmService;
        ICinemaService _cinemaService;
        IScreeningRoomService _screeningRoomService;
        IProgrammeService _programmeService;
        IHourService _hourService;
        IClientService _clientService;
        ITicketService _ticketService;
        IOrderService _orderService;

        public GlobalMenu(IFilmService filmService, ICinemaService cinemaService, IScreeningRoomService screeningRoomService, IProgrammeService programmeService, IHourService hourService, IClientService clientService, ITicketService ticketService, IOrderService orderService)
        {
            _filmService = filmService;
            _cinemaService = cinemaService;
            _screeningRoomService = screeningRoomService;
            _programmeService = programmeService;
            _hourService = hourService;
            _clientService = clientService;
            _ticketService = ticketService;
            _orderService = orderService;
        }

        private IoHelpers _ioHelper = new IoHelpers();

        public void CheckCondition(string option)
        {
            switch (option)
            {
                case "AddFilm":
                    Console.WriteLine("Wybrales opcje DODAJ FILM");
                    AddFilm();
                    break;
                case "AddCinema":
                    Console.WriteLine("Wybrales opcje DODAJ KINO");
                    AddCinema();
                    break;
                case "AddProgramme":
                    Console.WriteLine("Wybrales opcje DODAJ REPERTUAR");
                    AddProgramme();
                    break;
                case "PrintProgramme":
                    Console.WriteLine("Wybrales opcje WYSWIETL REPERTUAR");
                    DisplayProgramme();
                    break;
                case "ClientRegistration":
                    Console.WriteLine("Wybrales opcje REJESTRACJA KLIENTA");
                    ClientRegistration();
                    break;
                case "SellingTickets":
                    Console.WriteLine("Wybrales opcje SPRZEDAZ BILETOW");
                    SellingTickets();
                    break;
                case "PrintTickets":
                    Console.WriteLine("Wybrales opcje POKAZ BILETY ZAKUPIONE PRZEZ DANEGO UZYTKOWNIKA");
                    PrintTicketsForClient();
                    break;
            }
        }

        private void PrintTicketsForClient()
        {
            string mail = ConsoleUtilities.GetStringFromConsole("Podaj mail uzytkownika");
            _clientService.DoesEmailExistAsync(mail);

            var foundTickets = _clientService.GetAllTicketsForClientAsync(mail).Result;
            _ioHelper.PrintAllTickets(foundTickets);
        }

        private void PrintScreeningRoom(List<ProgrammeBl> programmes, TimeSpan seanceHour)
        {
            var foundScreeningRoom = _screeningRoomService.GetScreeningRoomForProgrammeAndHour(programmes, seanceHour);
            var letterOfTheLastRow = _screeningRoomService.GetLetterOfTheLastRow(foundScreeningRoom);
            _ioHelper.PrintScreeningRoomForCinema(foundScreeningRoom, letterOfTheLastRow);
        }

        private void PrintAllProgrammes()
        {
            var foundProgrammes = _programmeService.GetAllProgrammesAsync().Result;
            _ioHelper.PrintAllProgramme(foundProgrammes);
        }

        private void SellingTickets()
        { 
            var ticket = new TicketBl();
            var order = new OrderBl();
            List<TicketBl> tickets = new List<TicketBl>();

            PrintAllCinemas();

            var cinemaName = ConsoleUtilities.GetStringFromConsole("Podaj nazwe kina, w ktorym chcesz kupic bilet");
            ticket.Cinema = _programmeService.GetCinemaByNameAsync(cinemaName).Result;

            Console.WriteLine("Podaj interesujaca Cie date (MM/DD/YYYY)");
            ticket.DateTime = DateTime.ParseExact(Console.ReadLine(), "d", CultureInfo.InvariantCulture);

            var _programmes = _programmeService.GetProgrammeByCinemaNameAndDateAsync(ticket.Cinema, ticket.DateTime).Result;
            Console.WriteLine("Ponizej, dostepny repertuar:");
            _ioHelper.PrintProgramme(_programmes);

            ticket.Film = _filmService.GetMovieForProgramme(_programmes);

            string doYouWantBuyTicket = ConsoleUtilities.GetStringFromConsole("Wpisz Yes jesli chcesz kupic bilet");

            var userMail = ConsoleUtilities.GetStringFromConsole("Podaj mail uzytkownika");
            _clientService.DoesEmailExistAsync(userMail);
            ticket.Client = _clientService.GetClientByMailAsync(userMail).Result;

            var hourId = ConsoleUtilities.GetIntFromConsole("Podaj id godziny");
            ticket.Hour = _hourService.GetHourByIdAsync(hourId).Result;

            while (doYouWantBuyTicket == "Yes")
            {       
                ticket.Hour.ScreeningRoom = _screeningRoomService.GetScreeningRoomForProgrammeAndHour(_programmes, ticket.Hour.Hour);
                PrintScreeningRoom(_programmes, ticket.Hour.Hour);

                ticket.Row = ConsoleUtilities.GetStringFromConsole("Podaj rzad (litera)").ToUpper();
                ticket.Seat = ConsoleUtilities.GetIntFromConsole("Podaj miejsce (numer w rzędzie)");

                try
                {
                    _ticketService.IsLetter(ticket.Row);
                    _ticketService.IsRowExist(ticket.Hour.ScreeningRoom, ticket.Row);
                    _ticketService.IsSeatExist(ticket.Hour.ScreeningRoom, ticket.Seat);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.InnerException);
                }

                ticket.Price = _ticketService.CalculatePriceWithPropmotion(ticket.DateTime, userMail).Result;

                try
                {
                    order.AmountOfTheTicket += 1;
                    order.TotalPrice += ticket.Price;
                    tickets.Add(ticket);
                    int ticketId = _ticketService.AddTicketAsync(ticket).Result;
                    order.TicketsId.Add(ticketId);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                doYouWantBuyTicket = ConsoleUtilities.GetStringFromConsole("Wpisz Yes jesli chcesz kupic nastepny bilet");
            }
            try
            {
                _orderService.OrderAddAsync(order, tickets).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }        

        private void ClientRegistration()
        {
            string name = ConsoleUtilities.GetStringFromConsole("Podaj imie klienta");
            string surname = ConsoleUtilities.GetStringFromConsole("Podaj nazwisko klienta");
            string mail = ConsoleUtilities.GetStringFromConsole("Podaj adres e-mail");

            var client = new ClientBl()
            {
                Name = name,
                Surname = surname,
                MailAddress = mail
            };

            try
            {
                _clientService.AddClient(client).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void AddFilm()
        {
            string title = ConsoleUtilities.GetStringFromConsole("Podaj tytul filmu");

            _ioHelper.PrintGenre();

            var genre = (FilmBl.Genre)Enum.Parse(typeof(FilmBl.Genre), ConsoleUtilities.GetStringFromConsole("Podaj gatunek filmu"));

            int lengthOfTheFilm = ConsoleUtilities.GetIntFromConsole("Podaj czas trwania filmu w minutach");

            string description = ConsoleUtilities.GetStringFromConsole("Podaj krotki opis filmu");

            string website = ConsoleUtilities.GetStringFromConsole("Podaj adres do filmu na stronie filmweb");

            var film = new FilmBl()
            {
                Title = title,
                FilmGenre = genre,
                LengthOfTheFilm = lengthOfTheFilm,
                Description = description,
                Website = website,
            };

            try
            {
                _filmService.AddFilmAsync(film).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void AddCinema()
        {
            List<ScreeningRoomBl> screeningRoomsList = new List<ScreeningRoomBl>();

            string name = ConsoleUtilities.GetStringFromConsole("Podaj nazwe kina");
            string adress = ConsoleUtilities.GetStringFromConsole("Podaj adres");
            string mail = ConsoleUtilities.GetStringFromConsole("Podaj adres e-mail");
            int telephoneNumber = ConsoleUtilities.GetIntFromConsole("Podaj numer telefonu " +
                "(dopuszczalne formaty: xxxxxxxxx lub +xxxxxxxxxxx lub xxxxxxxxx");

            Console.WriteLine("Podaj godzine otwarcia kina (w formacie xx:xx)");
            var openTime = TimeSpan.ParseExact(Console.ReadLine(), "t", CultureInfo.InvariantCulture);

            var cinema = new CinemaBl()
            {
                Name = name,
                Adress = adress,
                Mail = mail,
                Telephone = telephoneNumber,
                OpenHour = openTime,
            };

            string doYouWantAddScreeningRoom = ConsoleUtilities.GetStringFromConsole("Wpisz Yes jesli chcesz dodac sale kinowa");

            while (doYouWantAddScreeningRoom == "Yes")
            {
                screeningRoomsList.Add(AddScreeningRoom(cinema));
                doYouWantAddScreeningRoom = ConsoleUtilities.GetStringFromConsole("Wpisz Yes jesli chcesz dodac nastepna sale kinowa");
            }

            cinema.ScreeningRooms = screeningRoomsList;
            try
            {
                _cinemaService.AddCinemaAsync(cinema).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void AddProgramme()
        {
            var programme = new ProgrammeBl();
            List<HourBl> hourBls = new List<HourBl>();

            Console.WriteLine("Dostepne kina:");
            PrintAllCinemas();

            var cinemaName = ConsoleUtilities.GetStringFromConsole("Podaj nazwe kina");
            programme.Cinema = _programmeService.GetCinemaByNameAsync(cinemaName).Result;

            Console.WriteLine("Dostepne filmy:");
            PrintAllMovies();

            var filmTitle = ConsoleUtilities.GetStringFromConsole("Podaj tytul filmu");
            programme.Film = _programmeService.GetMovieByTitleAsync(filmTitle).Result;

            UpdateDateRangeForProgramme(programme);

            string doYouWantAddHours = ConsoleUtilities.GetStringFromConsole("Wpisz Yes jesli chcesz dodac projekcje filmu");

            while (doYouWantAddHours == "Yes")
            {
                var hour = AddSeanceForProgramme(programme, cinemaName);
                hourBls.Add(hour);
                programme.Hours = hourBls;

                try
                {
                    _programmeService.DoesMovieIsPlayedBeforeCinemaOpening(programme);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                doYouWantAddHours = ConsoleUtilities.GetStringFromConsole("Wpisz Yes jesli chcesz dodac kolejna projekcje filmu");
            }
            try
            {
                _programmeService.AddProgrammeAsync(programme).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public HourBl AddSeanceForProgramme(ProgrammeBl programme, string cinemaName)
        {
            PrintSelectedScreeningRoom(programme);

            var roomNumber = ConsoleUtilities.GetIntFromConsole("Podaj numer sali");
            var screeningRoom = _programmeService.ChooseScreeningRoomsByNumberAsync(cinemaName, roomNumber).Result;


            Console.WriteLine("Podaj godzine seansu (w formacie xx:xx)");
            var seanceHour = TimeSpan.ParseExact(Console.ReadLine(), "t", CultureInfo.InvariantCulture);

            var hour = new HourBl()
            {
                Hour = seanceHour,
                ScreeningRoom = screeningRoom,
            };
            try
            {
                _programmeService.DoesMoviesTieInWithAsync(programme, hour, screeningRoom).Wait();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return hour;
        }

        public void PrintSelectedScreeningRoom(ProgrammeBl programme)
        {
            Console.WriteLine($"Kina: {programme.Cinema.Name} posiada nastepujace sale:");
            var screeningRooms = _cinemaService.FindScreenignRoomForCinemaAsync(programme.Cinema).Result;
            _ioHelper.PrintScreeningRoomForCinema(screeningRooms);
        }

        private void PrintAllCinemas()
        {
            var cinemas = _cinemaService.GetAllCinemasAsync().Result;
            _ioHelper.PrintCinemas(cinemas);
        }

        private void PrintAllMovies()
        {
            var movies = _filmService.GetAllMoviesAsync().Result;
            _ioHelper.PrintMovies(movies);
        }

        private void DisplayProgramme()
        {
            PrintAllCinemas();
            var currentData = DateTime.UtcNow;
            var cinema = ConsoleUtilities.GetStringFromConsole("Podaj nazwe kina dla ktorego chcesz sprawdzic repertuar");

            var cinemaName = _programmeService.GetCinemaByNameAsync(cinema).Result;

            var chooseDateForPrintingProgramme = ConsoleUtilities.GetStringFromConsole("Wpisz OBECNY jesli chcesz wyswietlic repertuar" +
                " na obecny tydzien lub wpisz INNY gdy chcesz wyswietlic repertuar dla innej daty");
            if (chooseDateForPrintingProgramme == "OBECNY")
            {
                try
                {
                    var _programmes = _programmeService.GetProgrammeByCinemaNameAndDateAsync(cinemaName, currentData).Result;
                    _ioHelper.PrintProgramme(_programmes);
                    ProgrammeExportToFile(_programmes);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            if (chooseDateForPrintingProgramme == "INNY")
            {
                try
                {
                    Console.WriteLine("Podaj date dla jakiej chcesz wyswietlic repertuar (MM/DD/YYYY)");
                    DateTime data = DateTime.ParseExact(Console.ReadLine(), "d", CultureInfo.InvariantCulture);
                    var _programmes = _programmeService.GetProgrammeByCinemaNameAndDateAsync(cinemaName, data).Result;
                    _ioHelper.PrintProgramme(_programmes);
                    ProgrammeExportToFile(_programmes);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            if (chooseDateForPrintingProgramme != "OBECNY" && chooseDateForPrintingProgramme != "INNY")
            {
                Console.WriteLine("Nie wybrales zadnej opcji");
            }
        }

        public void ProgrammeExportToFile(List<ProgrammeBl> programmes)
        {
            string chooseDoYouWantExportToFile = ConsoleUtilities.GetStringFromConsole("Wpisz Yes jesli chcesz wyeksportowac repertuar do pliku");

            while (chooseDoYouWantExportToFile == "Yes")
            {
                string path = ConsoleUtilities.GetStringFromConsole("Podaj sciezke w ktorej chcesz zapisac plik");

                try
                {
                    _programmeService.DoesThePathIsCorrect(path);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                string chosenFormatFile = ConsoleUtilities.GetStringFromConsole("Jesli chcesz eksportowac repertuar do pliku json, wpisz json, a jesli chcesz " +
                    "eksportowac repertuar do pliku xml, wpisz xml");
                if (chosenFormatFile == "json")
                {
                    _programmeService.ImportProgrammeToJson(programmes, path);
                }
                if (chosenFormatFile == "xml")
                {
                    _programmeService.ImportProgrammeToXml(programmes, path);
                }
                if (chosenFormatFile != "json" && chosenFormatFile != "xml")
                {
                    Console.WriteLine("nie wybrales poprawnego rozszerzenia pliku!");
                }
                chooseDoYouWantExportToFile = ConsoleUtilities.GetStringFromConsole("Wpisz Yes jesli chcesz ponownie wyeksportowac repertuar do pliku");
            }
        }

        private void UpdateDateRangeForProgramme(ProgrammeBl programme)
        {
            bool doesMovieWasPlayedBeforeInCinema = _programmeService.DoesMovieWasPlayedBeforeInCinemaAsync(programme).Result;

            if (doesMovieWasPlayedBeforeInCinema)
            {
                _programmeService.DetermineDateForProgrammeAsync(programme).Wait();
            }

            if (!doesMovieWasPlayedBeforeInCinema)
            {
                Console.WriteLine("Podaj date pierwszego dnia pierwszego tygodnia. Pamietaj ze tydzien zaczyna sie w piatek (MM/DD/YYYY)");
                DateTime startDate = DateTime.ParseExact(Console.ReadLine(), "d", CultureInfo.InvariantCulture);

                _programmeService.DetermineDateForProgramme(programme, startDate);
            }
        }

        private ScreeningRoomBl AddScreeningRoom(CinemaBl cinema)
        {
            int numberOfScreeningRoom = ConsoleUtilities.GetIntFromConsole("Podaj numer sali");
            int amountOfRow = ConsoleUtilities.GetIntFromConsole("Ile rzedow ma posiadac sala kinowa?");
            int amountOfSeatsPerRow = ConsoleUtilities.GetIntFromConsole("Ile miejsc ma posiadac jeden rzad?");

            var screeningRoom = new ScreeningRoomBl()
            {
                RoomNumber = numberOfScreeningRoom,
                AmountOfRow = amountOfRow,
                SeatsInRow = amountOfSeatsPerRow,
            };

            bool doesTheSameScreeningRoomExist = _cinemaService.DoesTheSameScreemingRoomExistAsync(screeningRoom, cinema).Result;
            bool isNegativeValueOfScreeningRoomNumber = _screeningRoomService.IsNegativeValueOfScreeningRoom(screeningRoom);

            try
            {
                bool isMoreRowThanLatinLetters = _screeningRoomService.IsMoreRowThanLatinLetters(amountOfRow);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            if (doesTheSameScreeningRoomExist)
            {
                Console.WriteLine("Sala o tym numerze juz istnieje w tym kinie.");
            }

            if (isNegativeValueOfScreeningRoomNumber)
            {
                Console.WriteLine("Numer sali nie moze byc ujemny");
            }
            else
            {
                return screeningRoom;
            }

            return screeningRoom;
        }
    }
}
