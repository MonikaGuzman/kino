﻿using Kino.Business.Interfaces;
using Kino.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Helpers
{
    public class IoHelpers
    {


        public void PrintMovies(IEnumerable<FilmBl> movies)
        {
            foreach (var movie in movies)
            {
                Console.WriteLine($"Id: {movie.Id}, Tytul: {movie.Title}, gatunek: {movie.FilmGenre}" +
                    $", czas trwania [min]: {movie.LengthOfTheFilm}, krotki opis: {movie.Description}" +
                    $", adres do strony {movie.Website} ");
            }
        }

        public void PrintCinemas(IEnumerable<CinemaBl> cinemas)
        {
            foreach (var cinema in cinemas)
            {
                Console.WriteLine($"Id: {cinema.Id}, Nazwa: {cinema.Name}, Adres: {cinema.Adress}" +
                    $", mail: {cinema.Mail}, telefon: {cinema.Telephone}");
            }
        }

        public void PrintScreeningRoomForCinema(IEnumerable<ScreeningRoomBl> screeningRooms)
        {
            foreach (var screeningRoom in screeningRooms)
            {
                Console.WriteLine($"Sala o numerze {screeningRoom.RoomNumber}, posiada {screeningRoom.AmountOfRow} rzedow" +
                    $", a w kazdym rzedzie jest {screeningRoom.SeatsInRow} miejsc");
            }
        }

        public void PrintScreeningRoomForCinema(ScreeningRoomBl screeningRoom, string letter)
        {
            Console.WriteLine($"Sala o numerze {screeningRoom.RoomNumber}, posiada rzedy od A do {letter}" +
                $", a w kazdym rzedzie jest {screeningRoom.SeatsInRow} miejsc");
        }

        public void PrintAllTickets(IEnumerable<TicketBl> tickets)
        {
            foreach (var ticket in tickets)
            {
                Console.WriteLine($"Tytul filmu: {ticket.Film.Title}"
                + $"\n Data i godzina seansu: {ticket.DateTime.ToShortDateString()} {ticket.Hour.Hour}" 
                + $"\n Kino i adres kina: {ticket.Cinema.Name} {ticket.Cinema.Adress}"
                + $"\n Rzad: {ticket.Row} i miejsce: {ticket.Seat}");
            }
        }

        public void PrintProgramme(IList<ProgrammeBl> programmes)
        {
            foreach (var programme in programmes)
            {
                Console.WriteLine($"Tytul filmu: {programme.Film.Title}"
                + $"\n Gatunek filmu: {programme.Film.FilmGenre}" + $"\n Krotki opis: {programme.Film.Description}"
                + $"\n czas trwania: {programme.Film.LengthOfTheFilm} [min]"
                + $"\n Godziny seansow id: {String.Join(",", programme.Hours.Select(x => x.Id))}"
                + $"\n Godziny seansow: {String.Join(",", programme.Hours.Select(x => x.Hour))}");
            }
        }

        public void PrintAllProgramme(IEnumerable<ProgrammeBl> programmes)
        {
            foreach (var programme in programmes)
            {
                Console.WriteLine($"Id repertuaru: {programme.Id}"
                + $"\n Kino: {programme.Cinema.Name}"
                + $"\n Tytul filmu: {programme.Film.Title}"
                + $"\n Gatunek filmu: {programme.Film.FilmGenre}" + $"\n Krotki opis: {programme.Film.Description}"
                + $"\n czas trwania: {programme.Film.LengthOfTheFilm} [min]"
                + $"\n Godziny seansow: {String.Join(",", programme.Hours.Select(x => x.Hour))}");
            }
        }

        public void PrintGenre()
        {
            string[] genres = Enum.GetNames(typeof(FilmBl.Genre));

            Console.WriteLine("dostepne gatunki filmu");

            foreach (var genre in genres)
            {
                Console.WriteLine(genre);
            }
        }
    }
}
