﻿using Kino.Business;
using Kino.Helpers;
using Ninject;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kino.Cli
{
    class Program
    {
        IKernel kernel = new StandardKernel(new ModuleBusiness());

        static void Main(string[] args)
        {
            string closeProgram = "";
            Console.WriteLine("Witaj w Kino");
            MapperConfig.CreateMap();
            while (closeProgram != "Exit")
            {
                DisplayMenu();
                string option = ConsoleUtilities.GetStringFromConsole("Jaka opcje wybierasz?");
                closeProgram = option;
                var program = new Program();
                try
                {
                    program.Start(option);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public void Start(string option)
        {
            var _globalMenu = kernel.Get<GlobalMenu>();
            _globalMenu.CheckCondition(option);
        }

        public static void DisplayMenu()
        {
            Console.WriteLine("Opcje do wyboru: ");
            Console.WriteLine("AddFilm");
            Console.WriteLine("AddCinema");
            Console.WriteLine("AddProgramme");
            Console.WriteLine("PrintProgramme");
            Console.WriteLine("ClientRegistration");
            Console.WriteLine("SellingTickets");
            Console.WriteLine("PrintTickets");
            Console.WriteLine("Exit");
        }
    }
}
